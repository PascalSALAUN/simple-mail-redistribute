#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export PATH



echo "# Step 1 : installing Python3 stuff"

#First update pip3
echo "Update pip" 
python3 -m pip install --upgrade pip


# Need virtualenv
echo  "Installing virtualenv"
pip3 install virtualenv


# Install all stuff in /usr/share/simpleMailRedistribute-p3
cd /usr/share/
echo "Installing all packages into \"/usr/share/simpleMailRedistribute-p3\" "
virtualenv -p /usr/bin/python3 simpleMailRedistribute-p3/
source simpleMailRedistribute-p3/bin/activate
pip3 install mail-parser PyYAML tzlocal pytz


echo ""
echo "# Step 2 : Configuring at minima simpleMailRedistribute"

echo "Enabling logs"
echo "Add /usr/share/simpleMailRedistribute/simpleMailRedistributeClass/log/scanlog.py if not done yet"
if [ ! -f /usr/share/simpleMailRedistribute/simpleMailRedistributeClass/log/scanlog.py ] ; then
    cp /usr/share/simpleMailRedistribute/simpleMailRedistributeClass/log/scanlog.py.sample /usr/share/simpleMailRedistribute/simpleMailRedistributeClass/log/scanlog.py
    echo "You can configure /usr/share/simpleMailRedistribute/simpleMailRedistributeClass/log/scanlog.py with your own values"
fi

echo ""
echo "Redirecting Logs from local5" 
echo "" >> /etc/rsyslog.conf
echo "#simpleMailRedistribute Logs" >> /etc/rsyslog.conf
echo "local5.*  -/var/log/simpleMailRedistribute/simpleMailRedistribute.log #simpleMailRedistribute" >> /etc/rsyslog.conf
sevice rsyslog restart


echo ""
echo "Define log rotation " 
if [ ! -f /etc/logrotate.d/simpleMailRedistribute ] ; then
    cp /usr/share/simpleMailRedistribute/scripts/sample/logrotate_simpleMailRedistribute.sample /etc/logrotate.d/simpleMailRedistribute
    echo "You can configure /etc/logrotate.d/simpleMailRedistribute with your own values"
fi


echo ""
echo "Edit new /etc/simpleMailRedistribute/simpleMailRedistribute.yml"
if [ ! -f /etc/simpleMailRedistribute/simpleMailRedistribute.yml ] ; then
    cp /etc/simpleMailRedistribute/simpleMailRedistribute.yml.sample /etc/simpleMailRedistribute/simpleMailRedistribute.yml
    echo "You have to configure /etc/simpleMailRedistribute/simpleMailRedistribute.yml with your own values"
fi


echo ""
echo "Setting cron" 
if [ ! -f /etc/cron.d/simpleMailRedistribute ] ; then
    cp /usr/share/simpleMailRedistribute/scripts/sample/cron_simpleMailRedistribute.sample /etc/cron.d/simpleMailRedistribute
    chmod 644 /etc/cron.d/simpleMailRedistribute
    echo "You can configure /etc/cron.d/simpleMailRedistribute with your own values"
fi


exit 0






