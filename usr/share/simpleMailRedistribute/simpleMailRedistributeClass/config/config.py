#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
 * ------------------------------------------------------------------------
 * simpleMailRedistribute
 * ------------------------------------------------------------------------
 * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>
 * @author  : Pascal SALAUN 
 * @Website : 
 * ------------------------------------------------------------------------
"""

# Native Python Lib/Class 
import yaml


class config():

    smrYml='/etc/simpleMailRedistribute/simpleMailRedistribute.yml'

    with open(smrYml, 'r') as stream:
        try:
            SMR = yaml.load(stream, Loader=yaml.FullLoader)
            stream.close()
        except yaml.YAMLError as exc:
            print(exc)
            pass
    stream.close()
    
    
