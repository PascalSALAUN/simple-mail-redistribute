#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
 * ------------------------------------------------------------------------
 * simpleMailRedistribute
 * ------------------------------------------------------------------------
 * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>
 * @author  : Pascal SALAUN 
 * @Website : http://www.simpleMailRedistribute.org <http://www.simpleMailRedistribute.org>
 * ------------------------------------------------------------------------
"""

# Native Python Lib/Class 
import yaml
cbYml='/etc/simpleMailRedistribute/simpleMailRedistribute.yml'

class _conf():

    def __init__(self):
        self.cfg = None
        with open(cbYml, 'r') as stream:
            try:
                self.cfg = yaml.load(stream, Loader=yaml.FullLoader)
            except yaml.YAMLError as exc:
                pass

    def get(self):        
        return self.cfg
