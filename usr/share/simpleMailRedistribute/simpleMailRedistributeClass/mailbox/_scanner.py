#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
 * ------------------------------------------------------------------------
 * simpleMailRedistribute
 * ------------------------------------------------------------------------
 * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>
 * @author  : Pascal SALAUN 
 * @Website : 
 * ------------------------------------------------------------------------
"""

from collections import defaultdict
from datetime import *
import base64
import imaplib
import json
import mailparser
import os
#from pytz import utc, timezone
import ssl
import time
#from email.utils import *




# simpleMailRedistribute inner class
from simpleMailRedistributeClass.config import config
from . import _sendMail

from simpleMailRedistributeClass.log import scanlog
backoffice = scanlog.scanlog().setLogger()

class _scanner():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params = defaultdict(dict)



    def setParam(self, **kwargs):
        """
        setParam:
        """            
        if "pid" in kwargs.keys():
            self.pid = kwargs['pid']
        
        # field : test in (sender address | subject) 
        if "filters" in kwargs.keys():
            self.filters = kwargs['filters']
            
        if "server" in kwargs.keys():
            self.params['server'] = kwargs['server']

        if "account" in kwargs.keys():
            self.params['account'] = kwargs['account']        
 
        if "passwd" in kwargs.keys():
            self.params['passwd'] = kwargs['passwd']           
        
        if "verifypeer" in kwargs.keys():
            self.params['verifypeer'] = kwargs['verifypeer']
        
        if "smtpPort" in kwargs.keys():
            self.params['smtpPort'] = kwargs['smtpPort']            
            
        if "smtpSsl" in kwargs.keys():
            self.params['smtpSsl'] = kwargs['smtpSsl']
            
            
            

    def scan(self,emlBytes):    
        status     = None
        scanStatus = None
        
        mail = mailparser.parse_from_bytes(emlBytes)
        scan     = defaultdict(dict)
          
        scan['message_id']  = mail.message_id.replace("<","").replace(">","")
        scan['subject']     = mail.subject       
        scan['date']        = mail.date
        scan['cType']       = mail.headers['Content-Type'].split('\n')[0].split(';')[0]
        
        i_log = '[' + self.pid + ']  :: scanning mail :: ' + scan['message_id']
        backoffice.info(i_log)
        
        fromAddress = mail.from_[0][1]
        
         
        if 'control' in self.filters:            
            for filtr in self.filters['control'].keys():                
                field        = self.filters['control'][filtr]['field']
                verb         = self.filters['control'][filtr]['verb']
                string       = self.filters['control'][filtr]['string']
                  
                if field == 'subject':
                    testText = mail.subject  
                
                if field == 'from':
                    testText = fromAddress
                
                ## We can test now
                if 'is' in verb:
                    if testText.lower() == string.lower():
                        scanStatus = "OK"
                        
                if 'contains' in verb:
                    if string.lower() in testText.lower():
                        scanStatus = "OK"   
                        
            
                if scanStatus == "OK":
                    scan['To'] = self.filters['control'][filtr]['distributeTo']
                    
        if 'forward' in self.filters.keys() and len(self.filters['forward']) > 0 :
                scanStatus = "OK"
                scan['forward'] = self.filters['forward']
                scan['to']      = mail.to + mail.from_
                if mail.cc:
                    scan['cc']      = mail.cc
                if mail.bcc:
                    scan['bcc']     = mail.bcc
                
                
        if scanStatus == "OK":
            scan['body'] = mail.body
            if mail.text_plain:
                scan['text_plain'] = mail.text_plain
                    
            if mail.text_html:   
                scan['text_html'] = mail.text_html
                    
                    
            scan['from'] = self.params['account']
            if mail.attachments:
                scan['attachments'] = mail.attachments

            sm = _sendMail._sendMail()
            sm.setParam(pid = self.pid, **self.params)
            status = sm.sendMail(**scan)
            if status is not None:
                status = scan['message_id']


        return status
