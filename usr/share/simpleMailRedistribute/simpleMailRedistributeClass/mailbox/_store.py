#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
 * ------------------------------------------------------------------------
 * simpleMailRedistribute
 * ------------------------------------------------------------------------
 * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>
 * @author  : Pascal SALAUN 
 * @Website : 
 * ------------------------------------------------------------------------
"""
import base64
from collections import defaultdict
import os, sys

# Promethium inner class
from simpleMailRedistributeClass.config import config
from simpleMailRedistributeClass.log import scanlog
backoffice = scanlog.scanlog().setLogger()




class _store():
    
    def __init__(self):
        self.params = defaultdict(dict)
        self.params['storePath'] = config.config().SMR['smrTempDir']

 
    def put(self, **kwargs):
        storePath = self.params['storePath'] + '/' + kwargs['message_id'] 
        
        if not os.path.isdir(storePath):
            os.makedirs(storePath)
        
        try:  
            fp = storePath + '/' + kwargs['filename']      
            payload = base64.b64decode(kwargs['payload'])

            with open(fp, 'wb') as f:
                f.write(payload)

            i_log = '[' + kwargs['filename'] + '] :: store file :: successful'
            backoffice.info(i_log)
                
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log =  '[' + kwargs['filename'] + '] :: store file :: failed'
            backoffice.info(i_log)
            pass        

       
    def get(self, **kwargs):
        storePath = self.params['storePath'] + '/' + kwargs['message_id']
        fp = storePath + '/' + kwargs['filename']   
      
        return open(fp, "rb").read()

    
    
    
    def delete(self, **kwargs):    
        storePath = self.params['storePath']  + '/' + kwargs['message_id']
        fp = storePath + '/' + kwargs['filename']    
        try:  
            if os.path.exists(fp):
                os.remove(fp) 
            i_log = 'removing file :: successful :: ' + fp
            backoffice.info(i_log)
            return True
                
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = 'removing file :: failed :: ' + fp
            backoffice.info(i_log)
            return False        
    
    
    def deleteSubDir(self, **kwargs):    
        storePath = self.params['storePath']  + '/' + kwargs['message_id']
        fp = storePath
        try:  
            if os.path.exists(fp):
                os.rmdir(fp) 
            i_log = 'removing subdir :: successful :: ' + fp
            backoffice.info(i_log)
            return True
                
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = 'removing subdir :: failed :: ' + fp
            backoffice.info(i_log)
            return False        

    
