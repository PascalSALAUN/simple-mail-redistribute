#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
 * ------------------------------------------------------------------------
 * simpleMailRedistribute
 * ------------------------------------------------------------------------
 * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>
 * @author  : Pascal SALAUN 
 * @Website : 
 * ------------------------------------------------------------------------
"""

# Python 3 
from collections import defaultdict
from datetime import *


from email.charset import Charset, QP


from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate, make_msgid
from email import encoders
import smtplib


# simpleMailRedistribute 
from . import _store
from simpleMailRedistributeClass.config import config
from simpleMailRedistributeClass.log import scanlog
backoffice = scanlog.scanlog().setLogger()


class _sendMail():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.params = defaultdict(dict)
        self.params['verifypeer'] = False


    def setParam(self, **kwargs):
        """
        setParam:
        """
        #print(kwargs)
        if "pid" in kwargs.keys():
            self.pid = kwargs['pid']
            
        if "server" in kwargs.keys():
            self.params['server'] = kwargs['server']

        if "account" in kwargs.keys():
            self.params['account'] = kwargs['account']        
 
        if "passwd" in kwargs.keys():
            self.params['passwd'] = kwargs['passwd']           
        
        if "verifypeer" in kwargs.keys():
            self.params['verifypeer'] = kwargs['verifypeer']
        
        if "smtpPort" in kwargs.keys():
            self.params['smtpPort'] = kwargs['smtpPort']            
            
        if "smtpSsl" in kwargs.keys():
            self.params['smtpSsl'] = kwargs['smtpSsl']
                

    def sendMail(self, **kwargs):        
        status = None
        html   = None
        CID    = False

        if 'alternative' or 'mixed' in kwargs['cType']:       
            msg = MIMEMultipart(kwargs['cType'].split('/')[1])
            if 'text_plain' in kwargs.keys():
                msg.attach(MIMEText(" ".join(kwargs['text_plain']), 'plain'))
                    
            if 'text_html' in kwargs.keys():
                if 'src="cid:' in ' '.join(kwargs['text_html']):
                    cs = Charset('utf-8')
                    cs.body_encoding = QP
                    html = MIMEMultipart('related')
                    html.attach(MIMEText(" ".join(kwargs['text_html']), 'html'))
                    CID = True

                else:
                    html =  MIMEText(" ".join(kwargs['text_html']), 'html')
        else:
            msg = MIMEMultipart()
            if 'text_html' in kwargs.keys():
                html = MIMEText(kwargs['text_html'], 'html')
            else:
                html = MIMEText(kwargs['body'], 'plain')

        msg['From']       = kwargs['from'] + '<' + kwargs['from'] +'>'
        
        if 'forward' in kwargs.keys():                   
            msg['dest']       = ",".join(kwargs['forward'])
            msg['Subject']    = '[Fwd] : ' + kwargs['subject']
            msg['To']         = ",".join([ tupl[0] + "<" + tupl[1] + ">" for tupl in kwargs['to'] ])
            if 'cc' in kwargs.keys():
                msg['Cc']         = ",".join([ tupl[0] + "<" + tupl[1] + ">" for tupl in kwargs['cc'] ])
            if 'bcc' in kwargs.keys():
                msg['Bcc']        = ",".join([ tupl[0] + "<" + tupl[1] + ">" for tupl in kwargs['bcc'] ])
        
        else:
            msg['dest']       = ",".join(kwargs['To'])
            msg['To']         = ",".join(kwargs['To'])
            msg['Subject']    = kwargs['subject']
        
        
        msg['message-id'] = make_msgid()
        #print(kwargs['date'])
        msg['Date']       = str(kwargs['date'])
        msg['References'] = '<' + kwargs['message_id'] + '>'
        msg['User-Agent'] = 'simpleMailRedistribute'
        
       
      
       
        if 'attachments' in kwargs.keys():
            stor = _store._store()
            for att in  kwargs['attachments']:               
                #dict_keys(['filename', 'payload', 'binary', 'mail_content_type', 'content-id', 'content-disposition', 'charset', 'content_transfer_encoding'])
                stor.put(message_id=msg['message-id'], **att)
                
                part = MIMEBase(att['mail_content_type'].split('/')[0], att['mail_content_type'].split('/')[1])
                part.set_payload(stor.get(message_id=msg['message-id'], filename=att['filename']))
                encoders.encode_base64(part)                

                part.add_header('Content-Disposition', att['content-disposition'])
                
                if 'content-id' in att.keys() and len(att['content-id']) > 0:
                    part.add_header('Content-ID', att['content-id'])
                    t_cid = 'src="cid:' + att['content-id'][1:-1] 
                stor.delete(message_id=msg['message-id'], filename=att['filename'])
                
                if CID  and  t_cid in ' '.join(kwargs['text_html']):
                    html.attach(part)
                else:
                    msg.attach(part)

            stor.deleteSubDir(message_id=msg['message-id'])
        
        if html is not None:
            msg.attach(html)

        text = msg.as_string() 
        
        try:
            server = smtplib.SMTP(self.params['server'], self.params['smtpPort'])
            server.starttls()
            server.login(self.params['account'], self.params['passwd'])
            server.sendmail(self.params['account'], msg['dest'], text)
            server.quit()
            
            status = "OK" 
            i_log = '[' + self.pid + ']  :: Send mail with subject : "' + kwargs['subject'] + '" succeeds with new message-id : ' + msg['message-id']
            backoffice.info(i_log)

        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = '[' + self.pid + ']  :: Send mail with subject : "' + kwargs['subject'] + '" failed'  
            backoffice.info(i_log)
            pass
        
        return status
