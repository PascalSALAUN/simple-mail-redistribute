#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
 * ------------------------------------------------------------------------
 * simpleMailRedistribute
 * ------------------------------------------------------------------------
 * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>
 * @author  : Pascal SALAUN 
 * @Website : 
 * ------------------------------------------------------------------------
"""

from collections import defaultdict
from datetime import *
import base64
import imaplib
import json
import mailparser
import os
from pytz import utc, timezone
import ssl
import time






# simpleMailRedistribute inner class
from simpleMailRedistributeClass.config import config
from . import _scanner



from simpleMailRedistributeClass.log import scanlog
backoffice = scanlog.scanlog().setLogger()

class _imap():
    def __init__(self):        
        """
            __init__ :
                Set default (dict) params
        """
        self.imap = None
        self.params = defaultdict(dict)
        self.params['verifypeer'] = False


    def setParam(self, **kwargs):
        """
        setParam:
        """
        #print(kwargs)      
        if "server" in kwargs.keys():
            self.params['server'] = kwargs['server']

        if "account" in kwargs.keys():
            self.params['account'] = kwargs['account']        
 
        if "passwd" in kwargs.keys():
            self.params['passwd'] = kwargs['passwd']           
        
        if "verifypeer" in kwargs.keys():
            self.params['verifypeer'] = kwargs['verifypeer']
        
        if "imapPort" in kwargs.keys():
            self.params['imapPort'] = kwargs['imapPort']            
            
        if "imapSsl" in kwargs.keys():
            self.params['imapSsl'] = kwargs['imapSsl']
            
        if "smtpPort" in kwargs.keys():
            self.params['smtpPort'] = kwargs['smtpPort']            
            
        if "smtpSsl" in kwargs.keys():
            self.params['smtpSsl'] = kwargs['smtpSsl']
            
        if "successfulBackupFolder" in kwargs.keys():
            self.params['successfulBackupFolder'] = kwargs['successfulBackupFolder']
        
        if "failedBackupFolder" in kwargs.keys():
            self.params['failedBackupFolder'] = kwargs['failedBackupFolder']
        
        # field : test in (subject | body | attachment) 
        if "filters" in kwargs.keys():
            self.params['filters'] = kwargs['filters']

            

    def connectSrv(self):
        if self.params['imapSsl'] :
            
            ctx = ssl.create_default_context()
            if self.params['verifypeer'] is False:
                ctx.check_hostname = False
                ctx.verify_mode = ssl.CERT_NONE          
                
            try:
                self.imap = imaplib.IMAP4_SSL(self.params['server'], self.params['imapPort'], ssl_context = ctx )

            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                return "Connection refused! Verify IMAPS parameters"
                #pass
                       
        else:
            try:
                self.imap = imaplib.IMAP4(self.params['server'], self.params['imapPort'])
            except Exception as e:
                backoffice.fatal(e, exc_info=True)
                return "Connection refused! Verify IMAP parameters"
                #pass



    def login(self):        
        self.pid = str(os.getpid())
        
        try: 
            self.imap.login(self.params['account'], self.params['passwd'])
            i_log = '[' + self.pid + ']  :: Connected to account : ' + self.params['account']
            backoffice.info(i_log)     
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = '[' + self.pid + ']  :: Connection failed to account : ' + self.params['account']
            backoffice.info(i_log)
            pass



    def test(self):
        self.pid = str(os.getpid())
        try:
            self.imap.login(self.params['account'], self.params['passwd'])
            i_log = '[' + self.pid + ']  :: TEST :: Connected to account : ' + self.params['account']
            backoffice.info(i_log)
            return "OK"
            self.imap.close()
        except Exception as e:
            backoffice.fatal(e, exc_info=True)
            i_log = '[' + self.pid + ']  :: Connection failed to account : ' + self.params['account']
            backoffice.info(i_log)
            return "KO"
            pass



    def retrieveMessages(self):            
        self.params['pid'] = self.pid
     
        self.imap.select('Inbox')
        tmp, data = self.imap.search(None, 'ALL')        
        tmpUl, uidL = self.imap.uid('search', None, "ALL")
       
        if self.params['successfulBackupFolder']:
            uidList = list(uidL[0].split())

            
        for num in data[0].split():
            # Only IMAP after
            typ, msg_data = self.imap.fetch(num,'(RFC822)')
            
            # Both POP/IMAP after
            s = _scanner._scanner()
            s.setParam(**self.params)
            message_id = s.scan(msg_data[0][1])
                
            # Only IMAP After
            # Copying message in INBOX subfolder in case of success or failure 
            if self.params['successfulBackupFolder'] and self.params['failedBackupFolder']:
                
                if message_id is None:
                    backupFolder = self.params['failedBackupFolder']
                else:
                    backupFolder = self.params['successfulBackupFolder']
                    
                muid = uidList[int(num)-1]
                result = self.imap.uid('COPY', muid, backupFolder)
                
                if result[0] == 'OK':
                    i_log = '[' + self.pid + ']  :: moving mail :: successful :: moved to ' + backupFolder
                    backoffice.info(i_log)
                    self.imap.uid('STORE', muid  , '+FLAGS', '(\Deleted)')
                else:
                    i_log = '[' + self.pid + ']  :: moving mail failed to ' + backupFolder
                    backoffice.error(i_log)
                
                time.sleep(1.5)
            
            
        if self.params['imapBackupFolder']:    
            self.imap.expunge()
 
        # Close for next time
        self.imap.close()


