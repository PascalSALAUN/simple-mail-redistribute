#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
 * ------------------------------------------------------------------------
 * simpleMailRedistribute
 * ------------------------------------------------------------------------
 * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>
 * @author  : Pascal SALAUN 
 * @Website : 
 * ------------------------------------------------------------------------
"""
# Native Python Lib/Class 
from collections import defaultdict
import os.path 
import sys
import yaml


sys.path.append(os.path.abspath("/usr/share/simpleMailRedistribute"))


# simpleMailRedistribute inner class
from simpleMailRedistributeClass.config import config
from simpleMailRedistributeClass.log import scanlog
backoffice = scanlog.scanlog().setLogger()

from simpleMailRedistributeClass.mailbox import _imap


ymlContent = defaultdict(dict)
ymlContent = config.config().SMR


if 'scan' in ymlContent.keys():
    for item in ymlContent['scan']:
        MBOX=item['mbox']
        msg = _imap._imap()       
        msg.setParam(**MBOX)
        msg.connectSrv()
        msg.login()
        msg.retrieveMessages()
